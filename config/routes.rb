Rails.application.routes.draw do
  root to: 'categories#show'
  get '/categories/(:id)' => 'categories#images'
end
