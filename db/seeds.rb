# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
(1..5).each do |i|
  Category.create name: "Category #{i}"
  (1..i*2).each { |j| Image.create category_id: i, url: "http://placehold.it/300?text=Image+#{j}+category+#{i}" }
end
