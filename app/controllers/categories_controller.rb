class CategoriesController < ApplicationController
  def show
  end

  def images
    urls = Category.find(params[:id]).images.pluck(:url)
    respond_to do |format|
      format.json { render json: urls }
    end
  end
end
