$ ->
  change_trigger = (handler) ->
    $.get
      url: "/categories/#{$('select').val()}"
      success: (data) ->
        box = $ '#images-box'
        # remove all old childs
        box.empty()
        # add each image of fetched category
        data.forEach (url) -> box.append "<img src=#{url}>"
  # Apply listener to categories
  # set id for that element if you want avoid of GOVNOCODE
  $('select').change change_trigger
  # Call trigger once to load picuters first time
  change_trigger $('select')
